-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-11-2016 a las 13:00:43
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ucv_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro`
--

CREATE TABLE `libro` (
  `idlibro` int(11) NOT NULL,
  `codigo` varchar(100) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `autor` varchar(255) DEFAULT NULL,
  `resena` text,
  `lugar` text,
  `foto` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libro`
--

INSERT INTO `libro` (`idlibro`, `codigo`, `nombre`, `autor`, `resena`, `lugar`, `foto`) VALUES
(1, 'L0001', 'La quinta disciplina', 'Peter Senge', 'La Quinta disciplina, el Pensamiento sistémico, busca que el equipo de trabajo y de esta forma la empresa en general se vea como un ente regido por sinergias, por fuerzas y por fortalezas, pero también por debilidades y amenazas. Evidenciarlas, entenderlas y apoyarse en las cosas buenas y buscar superar las malas. Tal cosa es lo que el Pensamiento sistémico busca, con el fin de funcionar cada vez mejor, con mayor eficacia, de forma consistente con los procesos y sistemas que rigen el mundo real y económico.', 'Stand A-1-1', 'quinta-disciplina.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `codigo` varchar(100) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `codigo`, `clave`, `nombre`) VALUES
(2, 'gaboo', 'e10adc3949ba59abbe56e057f20f883e', 'Gabriel Lozada'),
(3, 'bryan', 'e10adc3949ba59abbe56e057f20f883e', 'Bryan Sayan'),
(11, 'rmeza', 'e10adc3949ba59abbe56e057f20f883e', 'Ronney Meza'),
(12, 'alvaro', 'e10adc3949ba59abbe56e057f20f883e', 'Alvaro Mera'),
(13, 'ucv', 'e10adc3949ba59abbe56e057f20f883e', 'Universidad Cesar Vallejo'),
(14, 'andrew', 'e10adc3949ba59abbe56e057f20f883e', 'Andrew Mostacero');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`idlibro`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `libro`
--
ALTER TABLE `libro`
  MODIFY `idlibro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
