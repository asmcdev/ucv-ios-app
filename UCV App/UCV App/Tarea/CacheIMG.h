//
//  CacheIMG.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface CacheIMG : NSObject

+(void)descargaIMG:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;
+(void)descargaIMG:(NSURL *)url imageView:(UIImageView*)imageView;

@end
