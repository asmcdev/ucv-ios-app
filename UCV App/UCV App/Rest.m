//
//  Rest.m
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "Rest.h"
#import "DAO.h"
#import "Util.h"

@implementation Rest
static NSString *wsPath = @"http://asmc.space/ucv-ios/ws/"; //Ruta del webservice
static NSString *apiKey = @"UCVIOS2016"; //Clave del ApiRest

//Método para iniciar sesión desde el WS, mandamos el usuario y clave y obtenemos un obj "Usuario"
+(void)iniciarSesion: (NSString*) codigo clave: (NSString*)clave  callback:(void (^)(NSString *estado, NSString *msj, Usuario *usuario))callback{
    
    NSDictionary *param = @{@"_key": apiKey, @"codigo":codigo, @"clave":clave}; //Seteamos los parámetros al WS
    
    [self webService:@"iniciarSesion" param:param callback:^(NSArray *json, NSError *error) {
        
        Usuario *usuario = nil;
        NSString *estado = @"error";
        NSString *msj = @"";
        
        if(!error){
            if([json count]>0){ // Si retorna algún elemento, empleamos el sgte código:
                NSDictionary *jUsuario = [json objectAtIndex:0]; //Obtenemos el elemento en la posición 0, según el formato json
                msj = [jUsuario objectForKey:@"msj"]; //Obtenemos el valor del msj según JSON
                estado = [jUsuario objectForKey:@"estado"]; //Obtenemos el valor del estado según JSON
                
                if([estado isEqualToString:@"ok"]){ //Si el estado es OK, quiere decir que el usuario es válido
                    //Creamos el objeto usuario con sus atributos
                    usuario = [[Usuario alloc] init];
                    usuario.idusuario = [[jUsuario objectForKey:@"idusuario"] intValue];
                    usuario.codigo = [jUsuario objectForKey:@"codigo"];
                    usuario.clave = [jUsuario objectForKey:@"clave"];
                    usuario.nombre = [jUsuario objectForKey:@"nombre"];
                    [[DAO getInstancia] guardarUsuario:usuario]; //Guardamos al usuario en Sqlite
                }
            }else{
                estado = @"error";
                msj = @"Usuario y/o clave incorrectos";
            }
        }else{ // Si no retorna elemento alguno, al parecer ocurrió un error
            estado = @"error";
            msj = @"Ocurrió un error al iniciar sesión";
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(estado, msj, usuario); //Ejecutamos el callBack, es un método que se ejecuta después de haber realizado una determinada acción
        });
    }];
    
}

//Método para registrar o actualizar la información del usuario en el WS
+(void)guardarUsuario: (Usuario*) usuario  callback:(void (^)(NSString *estado, NSString *msj, Usuario *usuario))callback{
    
    NSDictionary *param = @{@"_key": apiKey, @"idusuario":[NSString stringWithFormat:@"%i",usuario.idusuario], @"codigo":usuario.codigo, @"clave":usuario.clave, @"nombre":usuario.nombre}; // Pasamos los parámetros
    
    [self webService:@"guardarUsuario" param:param callback:^(NSArray *json, NSError *error) {
        
        Usuario *usuario = nil;
        NSString *estado = @"error";
        NSString *msj = @"";
        
        if(!error){
            if([json count]>0){
                NSDictionary *jUsuario = [json objectAtIndex:0];
                msj = [jUsuario objectForKey:@"msj"];
                estado = [jUsuario objectForKey:@"estado"];
                
                if([estado isEqualToString:@"ok"]){
                    usuario = [[Usuario alloc] init];
                    usuario.idusuario = [[jUsuario objectForKey:@"idusuario"] intValue];
                    usuario.codigo = [jUsuario objectForKey:@"codigo"];
                    usuario.clave = [jUsuario objectForKey:@"clave"];
                    usuario.nombre = [jUsuario objectForKey:@"nombre"];
                    [[DAO getInstancia] guardarUsuario:usuario];
                }
            }else{
                estado = @"error";
                msj = @"Ocurrió un error al registrar usuario";
            }
        }else{
            estado = @"error";
            msj = @"Ocurrió un error al registrar usuario";
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(estado, msj, usuario);
        });
    }];
    
}

+(void)listarLibros: (NSString*) criterio  callback:(void (^)(NSMutableArray *lista))callback{
    
    NSString *ws = [NSString stringWithFormat:@"%@%@",wsPath,@"listarLibros"];
    NSURL *url = [NSURL URLWithString:ws];
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *params = [NSString stringWithFormat:@"_key=%@&criterio=%@", apiKey, [Util urlencode:criterio]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSMutableArray *lista = [[NSMutableArray alloc]init];
        NSError *wsError = nil;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&wsError];
        if([json count]>0){
            for (int i=0; i<[json count]; i++) {
                NSDictionary *jLibro = [json objectAtIndex:i];
                Libro *libro = [[Libro alloc] init];
                libro.idlibro = [[jLibro objectForKey:@"idlibro"] intValue];
                libro.codigo = [jLibro objectForKey:@"codigo"];
                libro.nombre = [jLibro objectForKey:@"nombre"];
                libro.autor = [jLibro objectForKey:@"autor"];
                libro.resena = [jLibro objectForKey:@"resena"];
                libro.lugar = [jLibro objectForKey:@"lugar"];
                libro.foto = [jLibro objectForKey:@"foto"];
                [[DAO getInstancia] guardarLibro:libro];
                [lista addObject:libro];
            }
        }
        
        callback(lista);
    }] resume];
    
}

+(void)webService: (NSString*) metodo param: (NSDictionary *)param  callback:(void (^)(NSArray *json, NSError *error))callback{
        
    NSString *parametros = @"";
    
    if(param!=nil){
        for(NSString *key in param){
            if([parametros isEqualToString:@""]){
                parametros = [NSString stringWithFormat:@"?%@=%@", key, [Util urlencode:param[key]]];
            }else{
                parametros = [NSString stringWithFormat:@"%@&%@=%@", parametros, key, [Util urlencode:param[key]]];
            }
        }
    }
    
    NSString *ws = [NSString stringWithFormat:@"%@%@%@",wsPath,metodo,parametros];
    NSURL *url = [NSURL URLWithString:ws];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
        completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
            NSError *wsError = nil;
            NSArray *json = nil;
            if(!error){
                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&wsError];
                if(wsError){
                    json = nil;
                }
            }else{
                wsError = error;
                json = nil;
            }
            callback(json, wsError);
                                                
    }];
    [task resume];
    
}

@end
