//
//  RegistroUsuarioViewController.h
//  UCV App
//
//  Created by docente on 23/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "BaseViewController.h"

@interface RegistroUsuarioViewController : BaseViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtNombre;
@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
@property (weak, nonatomic) IBOutlet UITextField *txtClave;

@end
