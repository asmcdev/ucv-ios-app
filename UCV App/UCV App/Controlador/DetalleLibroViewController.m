//
//  DetalleLibroViewController.m
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 6/11/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "DetalleLibroViewController.h"

@interface DetalleLibroViewController ()

@end

@implementation DetalleLibroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Libro *libro = (Libro *)self.obj;
    
    self.lblTitulo.text = libro.nombre;
    
    [CacheIMG descargaIMG:[NSURL URLWithString:libro.foto] imageView:self.foto];
    
    NSString *html = @"";
    html = [NSString stringWithFormat:@"%@<div style=\"font-size:14px;color:#FFFFFF;font-family:Arial;\">", html];
    html = [NSString stringWithFormat:@"%@ <b>Autor:</b> %@<br>", html, libro.autor];
    html = [NSString stringWithFormat:@"%@ <b>Lugar:</b> %@<br>", html, libro.lugar];
    html = [NSString stringWithFormat:@"%@ <b>Reseña:</b>%@<br>", html, @""];
    html = [NSString stringWithFormat:@"%@%@", html, libro.resena];
    html = [NSString stringWithFormat:@"%@</div>", html];
    
    
    [self contenidoHTML:html];
}
- (IBAction)retroceder:(id)sender {
    [self goToBack];
}

-(void)contenidoHTML: (NSString*)htmlString{
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    self.txtInfo.attributedText = attributedString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
