//
//  HomeViewController.h
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *lblBienvenido;

@end
