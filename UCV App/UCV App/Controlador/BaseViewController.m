//
//  BaseViewController.m
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dao = [DAO getInstancia];
    self.screenWidth = [[UIScreen mainScreen] bounds].size.width;
    self.screenHeight = [[UIScreen mainScreen] bounds].size.height;
}

- (void) goToViewController: (NSString *)controlador object: (NSObject *)object {
    BaseViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:controlador];
    [vc setObj:object];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) goToBack{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void) setUsuarioSesion: (int) idusuario{
    NSLog(@"Set usuario id: %i",idusuario);
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",idusuario] forKey:@"SesionUsuarioID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(Usuario *)getUsuarioSesion{
    NSString *idusuario = [[NSUserDefaults standardUserDefaults] valueForKey:@"SesionUsuarioID"];
    NSLog(@"get usuario id: %@",idusuario);
    if(idusuario!=nil && [idusuario intValue] > 0){
        Usuario *usuario = [[DAO getInstancia] buscarUsuarioxId:[idusuario intValue]];
        return usuario;
    }
    return nil;
}

-(void)mostrarProgress: (NSString*)mensaje{
    _progress = [[UIAlertView alloc] initWithTitle:mensaje message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [_progress show];
}

-(void)ocultarProgress{
    [_progress dismissWithClickedButtonIndex:-1 animated:YES];
}
-(void)temporizador: (int)segundos callback:(void (^)(bool estado))callback{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(segundos * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        callback(YES);
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
