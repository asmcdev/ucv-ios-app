//
//  HomeViewController.m
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Usuario *usuario = [self getUsuarioSesion];
    [_lblBienvenido setText:[NSString stringWithFormat:@"Bienvenido %@!", usuario.nombre]];
}
- (IBAction)salir:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Mensaje"
                                  message:@"¿Está seguro de salir?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* btnSi = [UIAlertAction
                            actionWithTitle:@"Si"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [alert dismissViewControllerAnimated:YES completion:nil];
                                [self setUsuarioSesion:0];
                                [self goToViewController:@"LoginVC" object:nil];
                            }];
    [alert addAction:btnSi];
    
    UIAlertAction* btnNo = [UIAlertAction
                            actionWithTitle:@"No"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [alert dismissViewControllerAnimated:YES completion:nil];
                            }];
    [alert addAction:btnNo];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}
- (IBAction)listarLibros:(id)sender {
    [self goToViewController:@"BibliotecaVC" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
