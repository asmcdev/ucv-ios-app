//
//  LoginViewController.m
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_txtClave.delegate = self;
    //_txtUsuario.delegate = self;
    //_scrollView.delegate = self;

}
- (IBAction)ingresar:(id)sender {
    
    [self mostrarProgress:@"Validando..."];
    
    NSString *codigo = [Util trim:_txtUsuario.text];
    NSString *clave = [Util trim:_txtClave.text];
    
    [Rest iniciarSesion:codigo clave:clave callback:^(NSString *estado, NSString *msj, Usuario *usuario) {
        [self ocultarProgress];
        if([estado isEqualToString:@"ok"]){
            [self setUsuarioSesion:usuario.idusuario];
            [self goToViewController:@"HomeVC" object:nil];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mensaje" message:msj delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (IBAction)registrar:(id)sender {
    [self goToViewController:@"RegistroUsuarioVC" object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
