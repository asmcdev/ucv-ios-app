//
//  DetalleLibroViewController.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 6/11/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "BaseViewController.h"

@interface DetalleLibroViewController : BaseViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *txtInfo;
@property (weak, nonatomic) IBOutlet UIImageView *foto;

@property (weak, nonatomic) IBOutlet UILabel *lblTitulo;



@end
