//
//  BaseViewController.h
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Rest.h"
#import "DAO.h"
#import "Util.h"
#import "CacheIMG.h"

@interface BaseViewController : UIViewController
@property DAO* dao;

@property int screenWidth;
@property int screenHeight;

@property NSObject *obj;

@property UIAlertView *progress;

- (void) goToViewController: (NSString *)controlador object: (NSObject *)object;
- (void) goToBack;

-(void) setUsuarioSesion: (int) idusuario;
-(Usuario *)getUsuarioSesion;

-(void)mostrarProgress: (NSString*)mensaje;
-(void)ocultarProgress;

-(void)temporizador: (int)segundos callback:(void (^)(bool estado))callback;

@end
