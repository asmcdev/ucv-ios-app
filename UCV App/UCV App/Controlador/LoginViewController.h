//
//  LoginViewController.h
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController<UITextFieldDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
@property (weak, nonatomic) IBOutlet UITextField *txtClave;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;




@end
