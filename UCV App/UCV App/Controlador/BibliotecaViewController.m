//
//  BibliotecaViewController.m
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "BibliotecaViewController.h"

@interface BibliotecaViewController ()

@end

@implementation BibliotecaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //delegate sirve para enlazar los métodos implementados de las clases abstractas a cada componente
    self.tblLibro.delegate = self;
    self.tblLibro.dataSource = self;
    
    //Obtenemos los libros del seridor y los listamos en la tabla
    [self temporizador:0 callback:^(bool estado) {
        [self cargarLibros];
    }];
}
- (IBAction)retroceder:(id)sender {
    [self goToBack];
}
- (IBAction)actualizar:(id)sender {
    [self cargarLibros];
}

- (void) cargarLibros {
    //Mostramos progrssBar
    [self mostrarProgress:@"Buscando libros..."];
    
    //Llamamos al método listarLibros por ApiRest
    [Rest listarLibros:@"" callback:^(NSMutableArray *lista) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self ocultarProgress];
            if([lista count] > 0){
                if (self.arrLibro != nil) {
                    self.arrLibro = nil;
                }
                self.arrLibro = lista; //Obtenemos el arreglo de libros
                [self.tblLibro reloadData]; //Recargamos la tabla
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mensaje" message:@"No se encontraron libros" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
                [alert show];
            }
        });
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1; //Deinimos las secciones para la grilla, en este caso es una celda por fila
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    long cant = 0; //Definimos en número de items que se van a listar, en este caso será la cantidad de items del arreglo
    if(self.arrLibro!=nil){
        cant = self.arrLibro.count;
    }
    return cant;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Obtenemos el prototipo de la fila y libro
    Libro *libro = [self.arrLibro objectAtIndex:indexPath.row]; //Obtenemos el libro en la posición X del arreglo
    CeldaLibro *cell = (CeldaLibro *)[tableView dequeueReusableCellWithIdentifier:@"idCeldaLibro"]; // Si la vista de la celda ya existe, solo la obtenemos, en caso contrario creamos la vista desde la clase CeldaLibro
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CeldaLibro" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    //Seteamos los valores del libro a los componentes
    cell.libro = libro;
    cell.lblTitulo.text = libro.nombre;
    cell.lblAutor.text = libro.autor;
    if(![[Util trim:libro.foto] isEqualToString:@""]){ // Si la foto del libro es diferente de vacío, la descargamos y mostramos
        [CacheIMG descargaIMG:[NSURL URLWithString:libro.foto] imageView:cell.imgFoto];
    }
    
    return cell; //Retornamos la vista de la celda
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Este método se emplea cuando hacemos "Tap" (presionamos) la celda
    
    Libro *libro = [self.arrLibro objectAtIndex:indexPath.row];
    NSLog(@"%i - %@", libro.idlibro, libro.nombre);
    [self goToViewController:@"DetalleLibroVC" object:libro];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0; //Definimos la altura de la celda (podemos definir una altura independiente de acuerdo a la posición X de la fila)
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
