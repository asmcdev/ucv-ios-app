//
//  Rest.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"
#import "Libro.h"
#import "Util.h"

@interface Rest : NSObject

+(void)iniciarSesion: (NSString*) codigo clave: (NSString*)clave  callback:(void (^)(NSString *estado, NSString *msj, Usuario *usuario))callback;
+(void)guardarUsuario: (Usuario*) usuario  callback:(void (^)(NSString *estado, NSString *msj, Usuario *usuario))callback;
+(void)listarLibros: (NSString*) criterio  callback:(void (^)(NSMutableArray *lista))callback;

@end
