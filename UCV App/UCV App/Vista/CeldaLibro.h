//
//  CeldaLibro.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 6/11/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Libro.h"

@interface CeldaLibro : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;
@property (nonatomic, weak) IBOutlet UILabel *lblAutor;
@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property Libro *libro;

@end
