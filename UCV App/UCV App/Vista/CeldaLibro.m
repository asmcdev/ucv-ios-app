//
//  CeldaLibro.m
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 6/11/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "CeldaLibro.h"

@implementation CeldaLibro

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
