//
//  RegistroUsuarioViewController.m
//  UCV App
//
//  Created by docente on 23/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "RegistroUsuarioViewController.h"

@interface RegistroUsuarioViewController ()

@end

@implementation RegistroUsuarioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)retroceder:(id)sender {
    [self goToBack];
}

- (IBAction)registrar:(id)sender {
    NSString *msj = @"";
    
    if([_txtNombre.text isEqualToString:@""]){
        msj = [NSString stringWithFormat:@"%@ %s", msj,"Debe ingresar nombre.\n"];
    }
    if([_txtUsuario.text isEqualToString:@""]){
        msj = [NSString stringWithFormat:@"%@ %s", msj,"Debe ingresar usuario.\n"];
    }
    if([_txtClave.text isEqualToString:@""]){
        msj = [NSString stringWithFormat:@"%@ %s", msj,"Debe ingresar clave.\n"];
    }
    
    if(![msj isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Advertencia" message:msj delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        
        Usuario *usuario = [Usuario new];
        usuario.nombre = _txtNombre.text;
        usuario.codigo = _txtUsuario.text;
        usuario.clave = _txtClave.text;
        usuario.idusuario = 0;
        
        [self mostrarProgress:@"Registrando usuario..."];
        
        [Rest guardarUsuario:usuario callback:^(NSString *estado, NSString *msj, Usuario *usuario) {
            [self ocultarProgress];
            if([estado isEqualToString:@"ok"]){
                [self setUsuarioSesion:usuario.idusuario];
                [self goToViewController:@"HomeVC" object:nil];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mensaje" message:msj delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
            [alert show];
        }];
    }
    
    
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
