//
//  Libro.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Libro : NSObject

@property int idlibro;
@property NSString* codigo;
@property NSString* nombre;
@property NSString* autor;
@property NSString* resena;
@property NSString* lugar;
@property NSString* foto;

@end
