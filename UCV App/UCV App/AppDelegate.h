//
//  AppDelegate.h
//  UCV App
//
//  Created by docente on 16/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

