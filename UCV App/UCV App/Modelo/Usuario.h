//
//  Usuario.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 23/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Usuario : NSObject

@property int idusuario;
@property NSString* codigo;
@property NSString* clave;
@property NSString* nombre;

@end
