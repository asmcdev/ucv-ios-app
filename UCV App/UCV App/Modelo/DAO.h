//
//  DAO.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 23/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Usuario.h"
#import "Libro.h"

@interface DAO : NSObject{
    NSString *databasePath;
}

+(DAO*)getInstancia;
-(BOOL)crearBD;
-(BOOL) guardarUsuario:(Usuario*)usuario;
-(Usuario*) buscarUsuarioxId:(int)idusuario;
-(Usuario*) buscarUsuarioxLogin:(NSString*)codigo clave:(NSString*)clave;
-(NSMutableArray*) listaUsuario;
-(BOOL) guardarLibro:(Libro*)libro;
-(Libro*) buscarLibroxId:(int)idlibro;


@end
