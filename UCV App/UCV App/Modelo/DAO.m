//
//  DAO.m
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 23/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "DAO.h"
#import "Util.h"

static DAO *instancia = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DAO

+(DAO*)getInstancia{
    if (!instancia) {
        instancia = [[super allocWithZone:NULL]init];
        [instancia crearBD];
    }
    return instancia;
}

-(BOOL)crearBD{
    NSString *docsDir;
    NSArray *dirPaths;
    // Obtener la carpeta de los documentos
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    NSLog(@"BD carpeta: %@", docsDir);
    // Crear el archivo de BD en la carpeta
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"ucv.db"]];
    BOOL ok = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            
            ok = [self ejecutarComando:@"CREATE TABLE USUARIO (idusuario INTEGER PRIMARY KEY   AUTOINCREMENT, codigo TEXT, clave TEXT, nombre TEXT)"];
            
            ok = ok && [self ejecutarComando:@"CREATE TABLE LIBRO (idlibro INTEGER PRIMARY KEY   AUTOINCREMENT, codigo TEXT, nombre TEXT, autor TEXT, resena TEXT, lugar TEXT, foto TEXT)"];
            
            /*Usuario *admin = [[Usuario alloc]init];
            admin.idusuario = 1;
            admin.codigo = @"admin";
            admin.clave = @"888888";
            admin.nombre = @"ADMIN";
            ok = [self guardarUsuario:admin];*/

            return  ok;
        }
        else {
            ok = NO;
            NSLog(@"Error al crear BD");
        }
    }
    return ok;
}

-(BOOL)conectar{
    BOOL ok = NO;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        ok = YES;
    }
    return ok;
}

-(BOOL)ejecutarComando:(NSString*)comando{
    BOOL ok = YES;
    char *err;
    if ([self conectar]){
        const char *sql_stmt = [comando UTF8String];
        if (sqlite3_exec(database, sql_stmt, NULL, NULL, &err)
            != SQLITE_OK)
        {
            ok = NO;
            NSLog(@"Error al ejecutar sql: %@",comando);
            sqlite3_close(database);
        }
    }else{
        ok = NO;
    }
    
    return ok;
}

-(BOOL) guardarUsuario:(Usuario*)usuario{
    BOOL ok = YES;
    if([self buscarUsuarioxId:usuario.idusuario]){
        ok = [self ejecutarComando:[NSString stringWithFormat:@"UPDATE USUARIO SET codigo='%@', clave='%@', nombre = '%@' WHERE idusuario = %i", [Util textoSQL:usuario.codigo], [Util textoSQL:usuario.clave], [Util textoSQL:usuario.nombre],usuario.idusuario ]];
    }else{
        if(usuario.idusuario == 0){
            ok = [self ejecutarComando:[NSString stringWithFormat:@"INSERT INTO USUARIO (codigo, clave, nombre) VALUES('%@', '%@', '%@')", [Util textoSQL:usuario.codigo], [Util textoSQL:usuario.clave], [Util textoSQL:usuario.nombre] ]];
        }else{
            ok = [self ejecutarComando:[NSString stringWithFormat:@"INSERT INTO USUARIO (codigo, clave, nombre, idusuario) VALUES('%@', '%@', '%@', %i)", [Util textoSQL:usuario.codigo], [Util textoSQL:usuario.clave], [Util textoSQL:usuario.nombre],usuario.idusuario ]];
        }
    }
    return ok;

}

-(Usuario*) buscarUsuarioxId:(int)idusuario{
    if ([self conectar])
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT idusuario, codigo, clave, nombre FROM USUARIO WHERE idusuario=%i",(int)idusuario];
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                Usuario *usuario = [Usuario new];
                usuario.idusuario = [[[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)] intValue];
                usuario.codigo = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 1)];
                usuario.clave = [[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 2)];
                usuario.nombre = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 3)];
                
                
                sqlite3_reset(statement);
                sqlite3_finalize(statement);
                return usuario;
            }
            else{
                
                sqlite3_reset(statement);
                sqlite3_finalize(statement);
                NSLog(@"Usuario x ID no encontrado");
                return nil;
            }
            
        }
    }
    return nil;
}

-(Usuario*) buscarUsuarioxLogin:(NSString*)codigo clave:(NSString*)clave{
    if ([self conectar])
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT idusuario, codigo, clave, nombre FROM USUARIO WHERE codigo='%@' AND clave = '%@'",codigo, clave];
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                Usuario *usuario = [Usuario new];
                usuario.idusuario = [[[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)] intValue];
                usuario.codigo = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 1)];
                usuario.clave = [[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 2)];
                usuario.nombre = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 3)];
                
                
                sqlite3_reset(statement);
                sqlite3_finalize(statement);
                return usuario;
            }
            else{
                
                sqlite3_reset(statement);
                sqlite3_finalize(statement);
                NSLog(@"Usuario x ID no encontrado");
                return nil;
            }
            
        }
    }
    return nil;
}

-(NSMutableArray*) listaUsuario{
    NSMutableArray *lista = [[NSMutableArray alloc]init];
    if ([self conectar])
    {
        NSString *querySQL = @"SELECT idusuario, codigo, clave, nombre FROM USUARIO";
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                Usuario *usuario = [Usuario new];
                usuario.idusuario = [[[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)] intValue];
                usuario.codigo = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 1)];
                usuario.clave = [[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 2)];
                usuario.nombre = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 3)];
                
                
                [lista addObject:usuario];
            }
            sqlite3_reset(statement);
            sqlite3_finalize(statement);
            
        }
    }
    return lista;
}





-(BOOL) guardarLibro:(Libro*)libro{
    BOOL ok = YES;
    if([self buscarLibroxId:libro.idlibro]){
        ok = [self ejecutarComando:[NSString stringWithFormat:@"UPDATE LIBRO SET codigo='%@', nombre='%@', autor = '%@', resena = '%@', lugar = '%@', foto = '%@' WHERE idlibro = %i", [Util textoSQL:libro.codigo], [Util textoSQL:libro.nombre], [Util textoSQL:libro.autor], [Util textoSQL:libro.resena], [Util textoSQL:libro.lugar], [Util textoSQL:libro.foto], libro.idlibro ]];
    }else{
        if(libro.idlibro == 0){
            ok = [self ejecutarComando:[NSString stringWithFormat:@"INSERT INTO LIBRO (codigo, nombre, autor, resena, lugar) VALUES('%@', '%@', '%@', '%@', '%@', '%@')", [Util textoSQL:libro.codigo], [Util textoSQL:libro.nombre], [Util textoSQL:libro.autor], [Util textoSQL:libro.resena], [Util textoSQL:libro.lugar], [Util textoSQL:libro.foto] ]];
        }else{
            ok = [self ejecutarComando:[NSString stringWithFormat:@"INSERT INTO LIBRO (codigo, nombre, autor, resena, lugar, foto, idlibro) VALUES('%@', '%@', '%@', '%@', '%@', '%@', %i)", [Util textoSQL:libro.codigo], [Util textoSQL:libro.nombre], [Util textoSQL:libro.autor], [Util textoSQL:libro.resena], [Util textoSQL:libro.lugar], [Util textoSQL:libro.foto], libro.idlibro ]];
        }
    }
    return ok;
    
}



-(Libro*) buscarLibroxId:(int)idlibro{
    if ([self conectar])
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"SELECT idlibro, codigo, nombre, autor, resena, lugar, foto FROM LIBRO WHERE idlibro=%i",(int)idlibro];
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                Libro *libro = [Libro new];
                libro.idlibro = [[[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 0)] intValue];
                libro.codigo = [[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 1)];
                libro.nombre = [[NSString alloc] initWithUTF8String:
                                (const char *) sqlite3_column_text(statement, 2)];
                libro.autor = [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 3)];
                libro.resena = [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 4)];
                libro.lugar = [[NSString alloc] initWithUTF8String:
                               (const char *) sqlite3_column_text(statement, 5)];
                
                sqlite3_reset(statement);
                sqlite3_finalize(statement);
                return libro;
            }
            else{
                sqlite3_reset(statement);
                sqlite3_finalize(statement);
                NSLog(@"Libro x ID no encontrado");
                return nil;
            }
            
        }
    }
    return nil;
}



@end
