//
//  Util.h
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

+(NSString*)textoSQL:(NSString*) valor;
+(NSString *)trim: (NSString*)valor;
+ (NSString *)urlencode: (NSString*)valor;

@end
