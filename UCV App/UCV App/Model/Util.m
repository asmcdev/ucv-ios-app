//
//  Util.m
//  UCV App
//
//  Created by Andrew Steven Mostacero Cabanillas on 30/10/16.
//  Copyright © 2016 Universidad Cesar Vallejo. All rights reserved.
//

#import "Util.h"

@implementation Util


+(NSString*)textoSQL:(NSString*) valor{
    return [valor stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
}
+(NSString *)trim: (NSString*)valor{
    valor = [valor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    valor = [valor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return valor;
}
+ (NSString *)urlencode: (NSString*)valor{
    NSMutableString *output = [NSMutableString string];             const unsigned char *source = (const unsigned char *)[valor UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
