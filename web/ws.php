<?php
	ini_set('max_execution_time', -1);
	ini_set('memory_limit', -1);
	ini_set('max_input_time', -1);
	date_default_timezone_set('America/Lima');
	//error_reporting(E_ALL);
	//ini_set("display_errors", 1);
	require_once("inc/Dao.php");
	require_once("inc/Rest.inc.php");

	class API extends REST {
		private $dao;
		private $ruta_base;
		private $impresora_activa = FALSE;

		public function __construct(){
			parent::__construct();
			$this->dao = new Dao();
			$this->ruta_base = str_replace('ws.php', '', 'http://' . $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
		}

		public function processApi(){
			$func = strtolower(trim(str_replace("/","",@$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0){
				$this->$func();
			} else {
				$this->response('',404);
			}
		}

		/**
		* Valida el Api Key para el WS
		*/
		public function validation_key($key) {
	        if ($key != 'UCVIOS2016') {
	            die('Acceso restringido');
	        }
	    }

		/**
		 * Envia fecha y hora del servidor
		 */
		private function fechahora(){
			$fechahora = date("Y-m-d h:i:s");
			$success = array('status' => "success", "fechahora" => $fechahora);
			$this->response($this->json($success),200);
		}

		/**
		 *	Codifica un array en formato JSON
		 */
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}



		/**
		* Función para iniciar sesión desde el webservice hacia la app
		*/
		public function iniciarSesion(){
			$codigo = @$_REQUEST['codigo'];
			$clave = @$_REQUEST['clave'];
			$key = @$_REQUEST['_key'];
			$this->validation_key($key);

			$login = $this->dao->getUsuarioxLogin($codigo, $clave);
			if ($login !== FALSE) {
				$result[0] = $login;
				$result[0]->estado = 'ok';
				$result[0]->msj = 'Inicio de sesion correcto';
			}else{
				$result = array(new stdClass);
				$result[0]->estado = 'error';
				$result[0]->msj = 'Datos de acceso incorrectos';
			}
			$result[0]->fechaservidor = date("Y-m-d h:i:s");
			$this->response($this->json($result),200);
		}

		public function listarUsuarios(){
			$result = array();
			$result = $this->dao->getListaUsuarios();
			$this->response($this->json($result),200);
		}

		public function getUsuario(){
			$result = array();
			$result['usuario'] = $this->dao->getUsuarioxId(@$_REQUEST['idusuario']);
			$this->response($this->json($result),200);
		}

		public function guardarUsuario(){
			$data = array();
			$data['idusuario'] = @$_REQUEST['idusuario'];
			$data['nombre'] = @$_REQUEST['nombre'];
			$data['codigo'] = @$_REQUEST['codigo'];
			$data['clave'] = @$_REQUEST['clave'];

			$result = array();
			if(!$this->dao->getUsuarioxCodigo($data['codigo'])){
				if($this->dao->guardarUsuario($data)){
					$login = $this->dao->getUsuarioxLogin($data['codigo'], $data['clave']);
					$result[0] = $login;
					$result[0]->estado = 'ok';
					$result[0]->msj = 'Usuario guardado';
				}else{
					$result[0] = new stdClass;
					$result[0]->estado = 'error';
					$result[0]->msj = 'Error al guardar usuario';
				}
			}else{
				$result[0] = new stdClass;
				$result[0]->estado = 'error';
				$result[0]->msj = 'El usuario ya existe';
			}
			$this->response($this->json($result),200);
		}

		public function listarLibros(){
			$criterio = @$_REQUEST['criterio'];
			$key = @$_REQUEST['_key'];
			$this->validation_key($key);

			$result = $this->dao->getListaLibros($criterio);
			foreach ($result as $k => $libro) {
				$libro->foto = "http://".$_SERVER['HTTP_HOST']."/ucv/upload/".$libro->foto;
			}
			$this->response($this->json($result),200);
		}
}
		$api = new API;
		$api->processApi();
