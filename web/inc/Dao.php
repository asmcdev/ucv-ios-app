<?php
session_start(); /* Sesión */

class Dao
{
    private $conex;
    private $bd_host    = "localhost";
    private $bd_usuario = "root";
    private $bd_clave   = "";
    private $bd_esquema = "ucv_bd";

    /* Constructor de la clase Dao() */
    public function __construct()
    {
        $this->conex = mysqli_connect($this->bd_host, $this->bd_usuario, $this->bd_clave, $this->bd_esquema);
        mysqli_query($this->conex, "SET NAMES 'utf8'");
    }

    /* Limpiar caracteres extraños */
    public function dato_seguro($valor)
    {
        return htmlspecialchars(mysqli_real_escape_string($this->conex, trim($valor)));
    }

    /* Obtener Usuario x ID */
    public function getUsuarioxId($idusuario)
    {
        $sql = "SELECT
        usu.idusuario,
        usu.codigo,
        usu.clave,
        usu.nombre
        FROM usuario usu usu.idusuario = '" . strtoupper($this->dato_seguro($idusuario)) . "'";

        $result = mysqli_query($this->conex, $sql);
        if (mysqli_num_rows($result) == 1) {
            return mysqli_fetch_object($result);
        } else {
            return false;
        }

    }

    /* Guardar usuario */
    public function guardarUsuario($data)
    {
        $sql = "";

        if((int)$data['idusuario']>0){
          $sql = "UPDATE usuario SET nombre = '".$this->dato_seguro($data['nombre'])."' ".((isset($data['clave']) && trim($data['clave'])!='')?(", clave = MD5('".$this->dato_seguro($data['clave'])."'), "):"")."
                                      WHERE idusuario = '".$data['idusuario']."'";
        }else{
          $sql = "INSERT INTO usuario (nombre, codigo, clave)
                              VALUES(
                                '".$this->dato_seguro($data['nombre'])."',
                                '".$this->dato_seguro($data['codigo'])."',
                                MD5('".$this->dato_seguro($data['clave'])."')
                              )";
        }

        $result = mysqli_query($this->conex, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    /* Obtener Usuario x Login */
    public function getUsuarioxLogin($codigo, $clave)
    {
        $sql = "SELECT
        usu.idusuario,
        usu.codigo,
        usu.clave,
        usu.nombre
      FROM usuario usu
      WHERE UPPER(usu.codigo) = '" . strtoupper($this->dato_seguro($codigo)) . "' AND UPPER(usu.clave) = UPPER(MD5('" . ($this->dato_seguro($clave)) . "'))";
        $result = mysqli_query($this->conex, $sql);
        if (mysqli_num_rows($result) > 0) {
            return mysqli_fetch_object($result);
        } else {
            return false;
        }
    }

    /* Obtener Usuario x Codigo */
    public function getUsuarioxCodigo($codigo)
    {
        $sql = "SELECT
        usu.idusuario,
        usu.codigo,
        usu.clave,
        usu.nombre
      FROM usuario usu
      WHERE UPPER(usu.codigo) = '" . strtoupper($this->dato_seguro($codigo)) . "' ";
        $result = mysqli_query($this->conex, $sql);
        if (mysqli_num_rows($result) > 0) {
            return mysqli_fetch_object($result);
        } else {
            return false;
        }
    }

    /* Obtener Usuarios */
    public function getListaUsuarios()
    {
        $sql = "SELECT
        usu.idusuario,
        usu.codigo,
        usu.clave,
        usu.nombre
      FROM usuario usu";
        $lista  = array();
        $result = mysqli_query($this->conex, $sql);

        while ($obj = mysqli_fetch_object($result)) {
            $lista[] = $obj;
        }

        return $lista;
    }

    /* Obtener Listado de Libros */
    public function getListaLibros($consulta = '')
    {
        $consulta = $this->dato_seguro($consulta);
        $sql = "SELECT idlibro, codigo, nombre, autor, resena, lugar, foto FROM libro WHERE nombre LIKE '%".$consulta."%'";
        $lista  = array();
        $result = mysqli_query($this->conex, $sql);

        while ($obj = mysqli_fetch_object($result)) {
            $lista[] = $obj;
        }

        return $lista;
    }


}
